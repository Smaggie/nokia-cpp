cmake_minimum_required(VERSION 2.8)
project(BTS_Tests)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set_gtest_options()

add_subdirectory(Application)
